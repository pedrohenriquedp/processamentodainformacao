#Um professor da UFABC decidiu que este ano irá calcular a média final dos seus alunos de PI fazendo a média aritmética das notas das listas de exercícios, excluindo-se a pior e a melhor nota. Assim, por exemplo, se ao longo do quadrimestre tal professor aplicar 5 avaliações nas quais um aluno pontue 5, 7.5, 3, 9 e 10, temos que a média final desse aluno será dada por (5 + 7.5 + 9) / 3 = 7.16.

#Escreva um programa que, dadas as notas de todas as listas, calcule a média final de um aluno.

#Entrada
#A entrada consiste de uma única linha contendo a nota que o aluno recebeu em cada uma das listas, separadas por espaço. Cada nota consiste de um número real e você pode assumir que o professor aplicou pelo menos 3 listas de exercício.

#Saída
#A saída do seu programa deve consistir de uma única linha contendo a frase “MEDIA FINAL:”, seguida da média final do aluno, separados por espaço. A média final deve ser apresentada com quatro casas decimais de precisão.

def notas():
    notasStr = input()

    todasNotas = notasStr.split(" ")

    valores = []

    for val in todasNotas:
        valores.append(float(val))

    soma=0
    for i in range(len(valores)):
        soma += valores[i]
        divisao = soma/len(valores)

    print(f"MEDIA FINAL: {divisao:.4f}")
resultado = notas()
