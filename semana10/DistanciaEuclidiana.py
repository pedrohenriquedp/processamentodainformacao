#Crie um programa que permita calcular a distância Euclidiana entre dois vetores.

#Entrada
#A entrada contém duas linhas representando dois vetores de números reais. Cada número é separado por um espaço em branco.

#Saída
#A distância Euclidiana entre ambos os vetores. O número real deve conter apenas 2 casas decimais. Caso a dimensão dos vetores forem diferentes a mensagem que deve ser impressa é “ERRO”, sem aspas.


def distancia():
    notasStr = input()
    notasStr2 = input()

    todasMedidas1 = notasStr.split(" ")
    todasMedidas2 = notasStr2.split(" ")

    valores1 = []
    valores2 = []

    for val in todasMedidas1:
        valores1.append(float(val))

    for val in todasMedidas2:
        valores2.append(float(val))

    if len(valores1) != len(valores2):
        print("ERRO")
    else:
        quadradoSoma = 0
        for i in range(len(valores1)):
            quadradoSoma += (valores1[i]-valores2[i])**2
            raizQuadrada = (quadradoSoma**(1/2))
        print(f"{raizQuadrada:.2f}")
resultado = distancia()
