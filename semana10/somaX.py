#Entrada
#A primeira linha da entrada consiste de um único número inteiro N (N ≥ 1) que indica o número de linhas da matriz de entrada. Os valores contidos na matriz são apresentados nas próximas N linhas da entrada, em que cada linha apresenta os valores contidos em uma linha da matriz (separados por espaço). Os valores contidos na matriz são números inteiros, possivelmente negativos.

#Saída
#A saída do seu programa deve consistir de uma única linha contendo a palavra “X = ”, seguida do valor da soma dos elementos no X da matriz.

#Dica
#Em vários exercícios desta lista, você deverá ler uma matriz de números inteiros cujos valores se encontram nas próximas N linhas e separados por espaços. Que tal criar uma função input_int_matrix(N: int) que lê e retorna tal matriz? Assim, nos próximos exercícios você poderá simplesmente colar o código dessa função e usá-la!
palavra = []

def fiqueiMuitoFeliz():
    n = int(input())
    for i in range(n):
        linha = list()
        for elemento in input().strip().split():
            linha.append(int(elemento))
        palavra.append(linha)

    elementos = []
    for linha in palavra:
        elementos.append(linha[::-1])

    colocando = -palavra[len(palavra)//2][len(palavra)//2]
    for i in range(len(palavra)):
        for j in range(len(palavra)):
            if i == j:
                colocando += palavra[i][j]
                colocando += elementos[i][j]
    print("X = ", colocando)

resultado = fiqueiMuitoFeliz()