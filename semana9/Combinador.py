#Implemente um programa denominado combinador, que recebe duas strings e deve combiná-las, alternando as letras de cada string, começando com a primeira letra da primeira string, seguido pela primeira letra da segunda string, em seguida pela segunda letra da primeira string, e assim sucessivamente. As letras restantes da cadeia mais longa devem ser adicionadas ao fim da string resultante e retornada.

#Entrada
#Cada caso de teste é composto por uma linha que contém duas cadeias de caracteres, separadas por um espaço em branco. Cada cadeia de caracteres deve conter entre 1 e 50 caracteres inclusive.

#Saída
#Combine as duas cadeias de caracteres da entrada como mostrado no exemplo abaixo e exiba a cadeia resultante.

mais1 = 0
vezes = 1
linha = input().split()
while mais1 < vezes:

    primeiraPal = linha[0]
    palavra_2 = linha[1]
    final_palavra = ""
    cont2 = 0

    while cont2 < len(primeiraPal) and cont2 < len(palavra_2):
        final_palavra += primeiraPal[cont2] + palavra_2[cont2]
        cont2 += 1

    if cont2 < len(primeiraPal):
        final_palavra += primeiraPal[cont2:]

    if cont2 < len(palavra_2):
        final_palavra += palavra_2[cont2:]

    print(final_palavra)
    mais1 += 1