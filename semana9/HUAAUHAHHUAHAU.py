#Em mensageiros eletrônicos, é muito comum entre jovens utilizar sequências de letras, que parecem muitas vezes aleatórias, para representar risadas e gargalhadas. Alguns exemplos comuns são:

#huaauhahhuahau
#
#hehehehe
#
#ahahahaha
#
#jaisjjkasjksjjskjakijs
#
#huehuehue
##
#Uma associação de psicologia ficou intrigada pela sonoridade das “risadas digitais”. Algumas delas nem mesmo se consegue pronunciar! Mas perceberam depois de muito estudo e testes inloco que algumas risadas parecem transmitir melhor o sentimento da risada que outras risadas.

def saida():
    entradaRisada = input()
    vogais = ""
    for i in entradaRisada:
        if(i=="a" or i=="e" or i=="i" or i=="o" or i=="u"):
            vogais+=i
    if(vogais == vogais[::-1]):
        print("S")
    else:
        print("N")

saida()
