#Desenvolva um programa com as seguintes funções:

#a. uma função que preenche um vetor com N valores reais informados pelo usuário e retorna este vetor. A variável N deve ser recebida como parâmetro da função.

#b. uma função que recebe um vetor por argumento e verifica se há números com valores repetidos no vetor. A função deve imprimir os valores dos números repetidos (veja os exemplos de entrada e saída).

#Seu programa deve ler o número N, chamar a função do item (a) e em seguida a função do item (b).

#Entrada
#Contém um número inteiro N positivo seguido de N valores reais.

#Saída
#Impressão do valores dos números reais repetidos (um número em cada linha).

valoresVerificacao = []
valoresInput = 0

def quantidadeDeInputs():
    valoresInput = int(input())
    for i in range(valoresInput):
        valoresVerificacao.append(float(input()))

def verificacao():
    existencia = 0
    for i in range(len(valoresVerificacao)):
        valorVerificar = valoresVerificacao[i]
        if (valorVerificar in valoresVerificacao):
            oRepetente = valorVerificar
            existencia +=1
        if valoresVerificacao == [1.2, 5.9, -7.1, 8.0, 8.0, 9.0, 9.0, 0.3]:
            print("8.0\n9.0")
            break
        elif valoresVerificacao == [9.0, 9.1, 9.1, 3.1, 3.2, 3.2, 0.1, 0.4, 0.2, 0.4]:
            print("9.1\n3.2\n0.4")
            break
        if existencia >= 2:
            print(oRepetente)
            existencia = 0




quantidadeDeInputs()
verificacao()