#Faça um programa que permita ler duas strings A e B e calcula quantas vezes B ocorre em A. Por exemplo, se A=“a arara bicou a jararaca” e B=“ara”, temos que B ocorre 4 vezes em A. Considere que o usuário digitará apenas strings sem acento.

#Entrada
#A entrada contém duas strings, sem acentos, que podem ter letras maiúsculas ou minúsculas, espaços e caracteres especiais.

#Saída
#Seu programa deve imprimir na tela quantas vezes a segunda string ocorre dentro da primeira string.



def padroes():
    quantidade = 0
    letraA = input()
    letraB = input()
    letraC = len(letraB)


    for i in range(len(letraA)-(letraC-1)):
        if (letraA[i:i+letraC]) == letraB:
            quantidade += 1

    print(quantidade)
resultado = padroes()