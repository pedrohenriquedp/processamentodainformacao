guiGuerrero = input().upper()
guiGuerreroLetras = []
cariocaGirls = 0
vogal = 0
espaco = 0
caractere = 0
consoante = 0

for i in guiGuerrero:
    guiGuerreroLetras.append(i)
    cariocaGirls +=1
    
contador = 0

for i in range(cariocaGirls):
    if guiGuerreroLetras[contador] == "A" or guiGuerreroLetras[contador] == "E" or guiGuerreroLetras[contador] == "I" or guiGuerreroLetras[contador] == "O" or guiGuerreroLetras[contador] == "U":
        vogal +=1
    elif guiGuerreroLetras[contador] == "." or guiGuerreroLetras[contador] == "," or guiGuerreroLetras[contador] == "!" or guiGuerreroLetras[contador] == "!" or guiGuerreroLetras[contador] == "?":
        caractere +=1
    elif guiGuerreroLetras[contador] == " ":
        espaco +=1
    else:
        consoante +=1
    contador +=1

print(f"Vogais: {((vogal/cariocaGirls)*100):.2f}%\nConsoantes: {((consoante/cariocaGirls)*100):.2f}%\nEspacos: {((espaco/cariocaGirls)*100):.2f}%\nPontuacoes: {((caractere/cariocaGirls)*100):.2f}%")
