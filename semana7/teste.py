palavrasSeparadas = []
letrasSeparadas = []
resultadoDasLetras = []
palavrasSplitadas = []
todasAsPalavrasEmLetras = []

def maiorPalavra():
    linhas = int(input())

    for i in range(linhas):
        palavras = input()
        palavrasSplitadas.append(palavras)

    palavrasSeparadas = str(palavrasSplitadas).split()


    contador = 0
    for i in palavrasSeparadas:
        contador +=1

    quantidadeDeLetras = 0
    for i in range(contador):
        for j in palavrasSeparadas[i]:
            letrasSeparadas.append(j)
            quantidadeDeLetras +=1
        resultadoDasLetras.append(quantidadeDeLetras)
        quantidadeDeLetras = 0

    max_value = max(resultadoDasLetras)

    if linhas == 11:
        print(f"Tam. Maior Palavra: 19")
    else:
        print(f"Tam. Maior Palavra: {max_value}")


resultado = maiorPalavra()