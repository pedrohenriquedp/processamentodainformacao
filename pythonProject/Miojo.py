def miojo():
    noodlesTime = int(input())
    hourGlass1 = int(input())
    hourGlass2 = int(input())

    totalTime = 0
    auxHourGlass1 = hourGlass1
    auxHourGlass2 = hourGlass2

    difference = None

    while(difference != 0):
        if(auxHourGlass1 > auxHourGlass2):
            difference = auxHourGlass1 - auxHourGlass2
            totalTime += auxHourGlass2
            auxHourGlass1 = difference
            auxHourGlass2 = hourGlass2

        else:
            difference = auxHourGlass2 - auxHourGlass1
            totalTime += auxHourGlass1
            auxHourGlass2 = difference
            auxHourGlass1 = hourGlass1

    if difference == noodlesTime:
        totalTime += noodlesTime
        return totalTime

    print(totalTime)

